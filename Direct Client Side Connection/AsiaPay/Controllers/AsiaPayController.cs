﻿using Com.Asiapay.Secure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AsiaPay.Controllers
{
    public class AsiaPayController : Controller
    {
        public ActionResult AsiaPayPage()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GenerateSecureHash(string merchantId, string orderRef, string currCode, string amount, string payType, string secureHashSecret)
        {
            try
            {
                string secureHash = null;
                SHAPaydollarSecure sha1 = new SHAPaydollarSecure();
                secureHash = sha1.generatePaymentSecureHash(
                    merchantId, orderRef, currCode, amount, payType, secureHashSecret);
                return Json(new
                {
                    success = true,
                    data = secureHash
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [Route("DataFeed")]
        public ActionResult DataFeed(string Ref, string ipCountry)
        {
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AsiaPay.Models;
using Com.Asiapay.Secure;


namespace AsiaPay.Controllers
{
    public class AsiaPayController : Controller
    {
        public ActionResult AsiaPayPage()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GenerateSecureHash(string merchantId, string orderRef, string currCode, string amount, string payType, string secureHashSecret)
        {
            try
            {
                string secureHash = null;
                SHAPaydollarSecure sha1 = new SHAPaydollarSecure();
                secureHash = sha1.generatePaymentSecureHash(
                    merchantId, orderRef, currCode, amount, payType, secureHashSecret);
                return Json(new
                {
                    success = true,
                    data = secureHash
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = false,
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [Route("DataFeed")]
        public ActionResult DataFeed()
        {
            string src = Request.Form["src"].ToString();//Return bank host status code (secondary).
            string prc = Request.Form["prc"].ToString();//Return bank host status code (primary).
            string successcode = Request.Form["successcode"].ToString();//0- succeeded, 1- failure, Others - error
            string Ref = Request.Form["Ref"].ToString();//Merchant‘s Order Reference Number
            string payRef = Request.Form["PayRef"].ToString();//PayDollar Payment Reference Number
            string amt = Request.Form["Amt"].ToString();//Transaction Amount
            string cur = Request.Form["Cur"].ToString();//Transaction Currency
            string payerAuth = Request.Form["payerAuth"].ToString();//Payer Authentication Status
            string ord = Request.Form["Ord"].ToString();//Bank Reference – Order id
            string holder = Request.Form["Holder"].ToString();//The Holder Name of the Payment Account
            string remark = Request.Form["remark"].ToString();//A remark field for you to store additional data that will not show on the transaction web page
            string authId = Request.Form["AuthId"].ToString();//Approval Code
            string eci = Request.Form["eci"].ToString();//ECI value (for 3D enabled Merchants)
            string sourceIp = Request.Form["sourceIp"].ToString();//IP address of payer
            string ipCountry = Request.Form["ipCountry"].ToString();//Country of payer ( e.g. HK) - if country is on high risk country list, an asterisk will be shown (e.g. MY*)
            string cardIssuingCountry = Request.Form["cardIssuingCountry"].ToString();//Card Issuing Country Code ( e.g. HK)
            string payMethod = Request.Form["payMethod"].ToString();//Payment method (e.g. VISA, Master, Diners, JCB, AMEX)
            string txTime = Request.Form["TxTime"].ToString();// Transaction Time
            string secureHashSecret = "GOk2ORdwy0b4rnyrfiRNVyBHMgT30UX8";//offered by paydollar
            SHAPaydollarSecure paydollarSecure = new SHAPaydollarSecure();
            string secureHashStr = Request.Form["secureHash"].ToString();
            bool verifyResult = false;
            if (secureHashStr != null && !"".Equals(secureHashStr))
            {
                if (secureHashStr.IndexOf(",") > 0)
                {
                    string[] secureHash = secureHashStr.Split(',');
                    for (int i = 0; i < secureHash.Length; i++)
                    {
                        verifyResult = paydollarSecure.verifyPaymentDatafeed(
                            src, prc, successcode, Ref, payRef, cur, amt, payerAuth, secureHashSecret, secureHash[i]);
                        if (verifyResult)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    verifyResult = paydollarSecure.verifyPaymentDatafeed(
                        src, prc, successcode, Ref, payRef, cur, amt, payerAuth, secureHashSecret, secureHashStr);
                }
            }
            if (!verifyResult)//verify secureHash result
            {
                var dataFeed = new DataFeedModel("SecureHash Verify Fail!");
                return View(dataFeed);
            }
            if ("0".Equals(successcode))
            {
                // Transaction Accepted
                // *** Add the Security Control here, to check the currency, amount with the 
                // *** merchant’s order reference from your database, if the order exist then 
                // *** accepted otherwise rejected the transaction.

                //  Update your database for Transaction Accepted and send email or notify your 
                //   customer.
                var dataFeed = new DataFeedModel(src, prc, successcode, Ref, payRef, amt, cur, payerAuth, ord, holder, remark, authId, eci, sourceIp, ipCountry, cardIssuingCountry, payMethod, txTime);
                return View(dataFeed);
            }
            else
            {
                // Transaction Rejected
                // Update your database for Transaction Rejected
                var dataFeed = new DataFeedModel("Transaction Rejected!");
                return View(dataFeed);
            }
        }
    }
}
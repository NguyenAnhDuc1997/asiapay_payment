﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AsiaPay.Models
{
    public class DataFeedModel
    {
        public DataFeedModel(string message)
        {
            Message = message;
        }

        public DataFeedModel(string src, string prc, string successcode, string @ref, string payRef, string amt, string cur, string payerAuth, string ord, string holder, string remark, string authId, string eci, string sourceIp, string ipCountry, string cardIssuingCountry, string payMethod, string txTime) : this(src)
        {
            Prc = prc;
            Successcode = successcode;
            Ref = @ref;
            PayRef = payRef;
            Amt = amt;
            Cur = cur;
            PayerAuth = payerAuth;
            Ord = ord;
            Holder = holder;
            Remark = remark;
            AuthId = authId;
            Eci = eci;
            SourceIp = sourceIp;
            IpCountry = ipCountry;
            CardIssuingCountry = cardIssuingCountry;
            PayMethod = payMethod;
            TxTime = txTime;
        }

        public string Src { get; set; }
        public string Prc { get; set; }
        public string Successcode { get; set; }
        public string Ref { get; set; }
        public string PayRef { get; set; }
        public string Amt { get; set; }
        public string Cur { get; set; }
        public string PayerAuth { get; set; }
        public string Ord { get; set; }
        public string Holder { get; set; }
        public string Remark { get; set; }
        public string AuthId { get; set; }
        public string Eci { get; set; }
        public string SourceIp { get; set; }
        public string IpCountry { get; set; }
        public string CardIssuingCountry { get; set; }
        public string PayMethod { get; set; }
        public string TxTime { get; set; }
        public string Message { get; set; }
    }
}
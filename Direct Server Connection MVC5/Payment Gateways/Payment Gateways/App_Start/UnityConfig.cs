using Payment_Gateways.Service;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Payment_Gateways
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            container.RegisterType<IAsiaPayService, AsiaPayService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
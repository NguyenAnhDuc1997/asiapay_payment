﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Com.Asiapay.Secure;
using System.Web.Mvc;
using Payment_Gateways.Model;
using System.Text;
using Payment_Gateways.Service;
using System.Collections;

namespace Payment_Gateways.Controllers
{
    public class AsiaPayController : Controller
    {
        private readonly IAsiaPayService _asiaPay;

        public AsiaPayController(IAsiaPayService asiaPay)
        {
            this._asiaPay = asiaPay;
        }

        // GET: AsiaPay
        public ActionResult AsiaPayDirectServer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AsiaPayDirectServer(string merchantId, string amount, string orderRef, string currCode, string pMethod, string cardNo, string securityCode, string cardHolder, string epMonth, string epYear, string payType, string lang, string secureHashSecret, string installment_service, string installment_period)
        {
            var getResult = _asiaPay.GetResult(lang, merchantId, orderRef, currCode, amount, payType, pMethod, cardNo, securityCode, epMonth, epYear, cardHolder, secureHashSecret, installment_service, installment_period);

            var resultMap = _asiaPay.Hashtable(getResult);

            if ("0".Equals((string)resultMap["successcode"]))
            {
                return Json(new
                {
                    success = true,
                    data = new { successcode = resultMap["successcode"], errMsg = resultMap["errMsg"], atm = resultMap["Amt"] }
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    data = getResult
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        [Route("DataFeed")]
        public ActionResult DataFeed(string errMsg, string successcode, string amt)
        {
            ViewBag.DataResult = errMsg + successcode + amt;
            return View();
        }
    }
}
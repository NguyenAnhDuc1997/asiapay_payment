﻿using Com.Asiapay.Secure;
using Payment_Gateways.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Payment_Gateways.Service
{
    public class AsiaPayService : IAsiaPayService
    {

        public string GetResult(string lang, string merchantId, string orderRef, string currCode, string amount, string payType, string pMethod, string cardNo, string securityCode, string epMonth, string epYear, string cardHolder, string secureHashSecret,string installment_service,string installment_period)
        {
            string secureHash = null;
            SHAPaydollarSecure sha1 = new SHAPaydollarSecure();
            secureHash = sha1.generatePaymentSecureHash(merchantId, orderRef, currCode, amount, payType, secureHashSecret);

            ServerPost serverPost = new ServerPost();
            StringBuilder postData = new StringBuilder();
            postData.Append("lang=" + lang);
            postData.Append("&merchantId=" + merchantId);
            postData.Append("&orderRef=" + orderRef);
            postData.Append("&currCode=" + currCode);
            postData.Append("&amount=" + amount);
            postData.Append("&payType=" + payType);
            postData.Append("&pMethod=" + pMethod);
            postData.Append("&cardNo=" + cardNo);
            postData.Append("&securityCode=" + securityCode);
            postData.Append("&epMonth=" + epMonth);
            postData.Append("&epYear=" + epYear);
            postData.Append("&cardHolder=" + cardHolder);
            postData.Append("&secureHash=" + secureHash);
            postData.Append("&installment_service=" + installment_service);
            postData.Append("&installment_period=" + installment_period);
            string result = serverPost.post(postData.ToString(), "https://test.paydollar.com/b2cDemo/eng/directPay/payComp.jsp");
            return result;
        }

        public Hashtable Hashtable(string getResult)
        {
            Hashtable resultMap = new Hashtable();
            string[] data = getResult.Split('&');
            for (int i = 0; data != null && i < data.Length; i++)
            {
                string[] tempData = data[i].Split('=');
                if (tempData == null)
                {
                    continue;
                }
                if (tempData.Length == 1)
                {
                    object key = new object();
                    key = tempData[0];
                    resultMap.Add(key, "");
                }
                else if (tempData.Length == 2)
                {
                    object key = new object();
                    key = tempData[0];
                    object value = new object();
                    value = tempData[1];
                    resultMap.Add(key, value);
                }
            }
            return resultMap;
        }
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment_Gateways.Service
{
    public interface IAsiaPayService
    {
        string GetResult(string lang, string merchantId, string orderRef, string currCode,
            string amount, string payType, string pMethod, string cardNo, string securityCode,
            string epMonth, string epYear, string cardHolder, string secureHashSecret, string installment_service, string installment_period);
        Hashtable Hashtable(string getResult);
    }
}
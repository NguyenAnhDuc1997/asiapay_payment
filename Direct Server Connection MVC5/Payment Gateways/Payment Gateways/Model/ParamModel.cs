﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payment_Gateways.Model
{
    public class ParamModel
    {
        public ParamModel()
        {
        }
        public string MerchantId { get; set; }
        public string Amount { get; set; }
        public string OrderRef { get; set; }
        public string CurrCode { get; set; }
        public string PaymentType { get; set; }
        public string Lang { get; set; }
        public string MpsMode { get; set; }
        public string PayMethod { get; set; }
        public string SecureHashSecret { get; set; }
        public string SecureHash { get; set; }
    }
}
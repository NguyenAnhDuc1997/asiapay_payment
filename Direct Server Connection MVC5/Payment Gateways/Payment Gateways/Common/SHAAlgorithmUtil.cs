﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Payment_Gateways.Common
{
    public class SHAAlgorithmUtil
    {
        public string operationAlgorithm(string secureData)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes_sha1_in = UTF8Encoding.Default.GetBytes(secureData);
            byte[] bytes_sha1_out = sha1.ComputeHash(bytes_sha1_in);
            string hexResult = BitConverter.ToString(bytes_sha1_out).Replace("-", string.Empty).ToLower();
            Console.WriteLine("hexResult=[" + hexResult + "]");
            return hexResult;
        }
    }
}